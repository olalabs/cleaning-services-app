package com.example.navigationbar;

import android.app.Activity;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;

import java.util.ArrayList;

public class CategoryAdapter extends ArrayAdapter<Category> {

    public CategoryAdapter(Activity context, ArrayList<Category> categories){
        super(context, 0, categories); //konstruktor nadklasy
    }

    @NonNull
    @Override
    public View getView(int position, @Nullable View convertView, @NonNull ViewGroup parent){

        View listItemView = convertView;

        if(listItemView == null){
            listItemView = LayoutInflater.from(getContext()).inflate(R.layout.list_item, parent, false);
        }

        final Category currentCategory = getItem(position);

        TextView name = listItemView.findViewById(R.id.name);
        name.setText(currentCategory.getName());

        TextView price = listItemView.findViewById(R.id.price);
        price.setText("£" + currentCategory.getPrice());

        return listItemView;
    }
}

package com.example.navigationbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

public class HomeFragment extends Fragment implements View.OnClickListener {
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_home, container, false);
        Button button = view.findViewById(R.id.button_call);
        button.setOnClickListener(this);
        getActivity().setTitle("About us");
        return view;
    }

    @Override
    public void onClick(View v){
        call(v);
    }

    public void call(View view){
        //action_dial allows the user to decide whether to actually make the call or not
        //it does not require the call_phone permission
        Intent intent = new Intent(Intent.ACTION_DIAL, Uri.parse("tel:" + "800 345 759 "));
        startActivity(intent);
    }
}

package com.example.navigationbar;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

public class MessageFragment extends Fragment implements View.OnClickListener {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_message, container, false);
        Button button = view.findViewById(R.id.button_send);
        button.setOnClickListener(this);
        getActivity().setTitle("Send email to our team");
        return view;
    }

    @Override
    public void onClick(View v){
        sendMessage(v);
    }

    public void sendMessage(View view){
        String[] recipients = insertRecipients();
        String subject = insertSubject();
        String message = insertMessage();
        sendEmail(recipients, subject, message);
    }

    private String[] insertRecipients(){
        List<String> emails = new ArrayList<>();
        emails.add("mwojcik95@gmail.com");
        emails.add("marylu095@wp.pl");
        return emails.toArray(new String[0]);
    }

    private String insertSubject(){
        EditText editText = getView().findViewById(R.id.edit_text_subject);
        return editText.getText().toString();
    }

    private String insertMessage(){
        EditText editText = getView().findViewById(R.id.edit_text_message);
        return editText.getText().toString();
    }

    private void sendEmail(String[] recipients, String subject, String message){
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:"));
        intent.putExtra(Intent.EXTRA_EMAIL, recipients);
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT, message);
        if(intent.resolveActivity(getActivity().getPackageManager()) != null){
            startActivity(intent);
        }

    }
}

package com.example.navigationbar;

import android.app.DatePickerDialog;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class OrderFragment extends Fragment implements View.OnClickListener, DatePickerDialog.OnDateSetListener {

    private List<Integer> spinnersIds = new ArrayList<>();
    private List<Integer> spinnersOptions = new ArrayList<>();

    private static final String TAG = "OrderFragment";
    private TextView displayDate;
    private DatePickerDialog.OnDateSetListener mDateSetListener;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        getActivity().getWindow().setBackgroundDrawableResource(R.drawable.background);



        insertSpinnersId();
        insertSpinnersOptions();

        View view = inflater.inflate(R.layout.fragment_order, container, false);



        loadSpinnerOptions(view, container, spinnersIds.get(0), spinnersOptions.get(0));
        loadSpinnerOptions(view, container, spinnersIds.get(1), spinnersOptions.get(1));



        //LinearLayout orderForm = (LinearLayout) getActivity().findViewById(R.id.order_form);
        // addressForm = (LinearLayout) getActivity().findViewById(R.id.address_form);

        //orderForm.setVisibility(View.VISIBLE);
        //addressForm.setVisibility(View.GONE);


        this.displayDate = view.findViewById(R.id.date);
        this.displayDate.setOnClickListener(this);

        Button resume = view.findViewById(R.id.button_result);
        resume.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                updateSummary();
            }
        });

        getActivity().setTitle("Order the service");

        return view;
    }

    @Override
    public void onClick(View v) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DAY_OF_MONTH);

        DatePickerDialog dialog = new DatePickerDialog(
                getActivity(),
                android.R.style.Theme_Holo_Light_Dialog_MinWidth,
                this,
                year, month, day);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        dialog.show();
    }

    @Override
    public void onDateSet(DatePicker datePicker, int year, int month, int day) {
        month = month + 1;
        String date = month + "/" + day + "/" + year;
        displayDate.setText(date);
    }

    private void loadSpinnerOptions(View view, ViewGroup container, int id, int option) {
        Spinner spinner = view.findViewById(id);
        ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(container.getContext(), option, android.R.layout.simple_spinner_item);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
    }

    private void insertSpinnersId() {
        this.spinnersIds.add(R.id.option_one);
        this.spinnersIds.add(R.id.option_two);
    }

    private void insertSpinnersOptions() {
        this.spinnersOptions.add(R.array.type);
        this.spinnersOptions.add(R.array.conservatory);
    }


    private void updateSummary() {
        int summaryPrice = 0;

        Spinner spinner = getActivity().findViewById(R.id.option_two);
        if (spinner.getSelectedItem().toString().equals("big")) {
            summaryPrice += 15;
        } else if (spinner.getSelectedItem().toString().equals("medium")) {
            summaryPrice += 10;
        } else if (spinner.getSelectedItem().toString().equals("small")) {
            summaryPrice += 5;
        }

        EditText editText = getActivity().findViewById(R.id.edit_text_roof);
        if (!editText.getText().toString().isEmpty()) {
            summaryPrice += (2 * Integer.parseInt(editText.getText().toString()));
        }

        editText = getActivity().findViewById(R.id.edit_text_bay);
        if (!editText.getText().toString().isEmpty()) {
            summaryPrice += (2 * Integer.parseInt(editText.getText().toString()));
        }

        editText = getActivity().findViewById(R.id.edit_text_other);
        if (!editText.getText().toString().isEmpty()) {
            summaryPrice += (1 * Integer.parseInt(editText.getText().toString()));
        }

        if (summaryPrice != 0) {
            summaryPrice += 11;
        }

        TextView name = getActivity().findViewById(R.id.summary);
        name.setText("AMOUNT TO PAY:  £" + summaryPrice);
    }



}

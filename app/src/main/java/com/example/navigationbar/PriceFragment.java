package com.example.navigationbar;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import java.util.ArrayList;

public class PriceFragment extends Fragment {

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.price_list, container, false);

        final ArrayList<Category> tariff = new ArrayList<>(); //const

        String[] serviceNames = getResources().getStringArray(R.array.services);
        int[] servicePrices = getResources().getIntArray(R.array.prices);
        for (int i = 0; i < serviceNames.length; i++) {
            tariff.add(new Category(serviceNames[i], servicePrices[i]));
        }

        CategoryAdapter adapter = new CategoryAdapter(getActivity(), tariff);

        ListView listView = rootView.findViewById(R.id.list);
        listView.setAdapter(adapter);
        getActivity().setTitle("Price list");

        return rootView;
    }
}
